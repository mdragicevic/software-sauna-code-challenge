package softwaresauna.challenge.assignment;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import softwaresauna.challenge.assignment.model.ChallengeResult;
import softwaresauna.challenge.assignment.util.FolderEnum;

@SpringBootTest
class AcceptanceTests extends TestBase {

    @Test
    @DisplayName("Test 1 - A basic example")
    void test01() throws Exception {
        Integer testIndex = 1;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_ACCEPTANCE);
        ChallengeResult result = mainService.processInput(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 2 - Go straight through intersections")
    void test02() throws Exception {
        Integer testIndex = 2;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_ACCEPTANCE);
        ChallengeResult result = mainService.processInput(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 3 - Letters may be found on turns")
    void test03() throws Exception {
        Integer testIndex = 3;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_ACCEPTANCE);
        ChallengeResult result = mainService.processInput(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 4 - Do not collect a letter from the same location twice")
    void test04() throws Exception {
        Integer testIndex = 4;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_ACCEPTANCE);
        ChallengeResult result = mainService.processInput(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 5 - Keep direction, even in a compact space")
    void test05() throws Exception {
        Integer testIndex = 5;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_ACCEPTANCE);
        ChallengeResult result = mainService.processInput(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 6 - Ignore stuff after end of path")
    void test06() throws Exception {
        Integer testIndex = 6;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_ACCEPTANCE);
        ChallengeResult result = mainService.processInput(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 7 - Missing start character")
    void test07() {
        Integer testIndex = 7;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 8 - Missing end character")
    void test08() {
        Integer testIndex = 8;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 9 - Multiple starts 1")
    void test09() {
        Integer testIndex = 9;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 10 - Multiple starts 2")
    void test10() {
        Integer testIndex = 10;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 11 - Multiple starts 3")
    void test11() {
        Integer testIndex = 11;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 12 - Fork in path")
    void test12() {
        Integer testIndex = 12;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 13 - Broken path")
    void test13() {
        Integer testIndex = 13;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 14 - Multiple starting paths")
    void test14() {
        Integer testIndex = 14;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }

    @Test
    @DisplayName("Test 15 - Fake turn")
    void test15() {
        Integer testIndex = 15;
        executeTestThatValidatesError(testIndex, FolderEnum.INPUT_ACCEPTANCE);
    }


}
