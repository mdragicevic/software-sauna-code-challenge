package softwaresauna.challenge.assignment.util;

public enum FolderEnum {

    INPUT_ACCEPTANCE("inputs/acceptance"),
    RESULT_ACCEPTANCE("results/acceptance"),
    INPUT_UNIT("inputs/unit"),
    RESULT_UNIT("results/unit");
    private final String name;

    FolderEnum(String name) {
        this.name = name;
    }


    public String getName() {
        return this.name;
    }
}
