package softwaresauna.challenge.assignment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import softwaresauna.challenge.assignment.model.ChallengeResult;
import softwaresauna.challenge.assignment.services.MainService;
import softwaresauna.challenge.assignment.util.FolderEnum;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class TestBase {

    @Autowired
    protected MainService mainService;
    ObjectMapper objectMapper = new ObjectMapper();

    public static String fetchFile(Integer testIndex, FolderEnum folder) {
        String response = null;
        try {
            String fileLocation = "classpath:" + folder.getName() + "/" + testIndex;
            File file = ResourceUtils.getFile(fileLocation);
            InputStream inputStream = new FileInputStream(file);
            response = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    void executeTestThatValidatesError(Integer testIndex, FolderEnum folder) {
        String input = TestBase.fetchFile(testIndex, folder);
        String errorMessage = "No exception occured";
        try {
            mainService.processInput(input);
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
        validateTestError(testIndex, errorMessage);
    }

    protected void validateTestError(Integer testIndex, String actualErrorMessage) {
        String expectedErrorMessage = TestBase.fetchFile(testIndex, FolderEnum.RESULT_ACCEPTANCE);
        Assertions.assertEquals(expectedErrorMessage.trim(), actualErrorMessage);
    }

    protected void validateTestResult(Integer testIndex, Object result, FolderEnum folder) throws JsonProcessingException {
        String actualResult = objectMapper.writeValueAsString(result);
        String expectedResult = TestBase.fetchFile(testIndex, folder);
        Assertions.assertEquals(objectMapper.readTree(expectedResult), objectMapper.readTree(actualResult));
    }

}


