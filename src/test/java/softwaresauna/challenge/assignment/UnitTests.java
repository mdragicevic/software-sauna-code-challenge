package softwaresauna.challenge.assignment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import softwaresauna.challenge.assignment.model.ChallengeResult;
import softwaresauna.challenge.assignment.model.MatrixElement;
import softwaresauna.challenge.assignment.services.MappingService;
import softwaresauna.challenge.assignment.services.MovementService;
import softwaresauna.challenge.assignment.services.RecordingService;
import softwaresauna.challenge.assignment.util.FolderEnum;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringBootTest
class UnitTests extends TestBase {

    @Autowired
    private MappingService mappingService;
    @Autowired
    private MovementService movementService;
    @Autowired
    private RecordingService recordingService;
    private List<List<MatrixElement>> matrix;

    @Test
    @DisplayName("Test 1 - test input mapping")
    void test01() throws Exception {
        Integer testIndex = 1;
        String input = TestBase.fetchFile(testIndex, FolderEnum.INPUT_UNIT);
        List<List<MatrixElement>> result = mappingService.mapTextToMatrix(input);
        validateTestResult(testIndex, result, FolderEnum.RESULT_UNIT);
    }

    @Test
    @DisplayName("Test 2 - find starting position")
    void test02() {
        MatrixElement foundElement = movementService.findStartingPosition();
        Assertions.assertEquals('@', (char) foundElement.getContent());
    }

    @Test
    @DisplayName("Test 3 - find next step")
    void test03() throws Exception {
        Integer testIndex = 3;
        String elementString = TestBase.fetchFile(testIndex, FolderEnum.INPUT_UNIT);
        MatrixElement element = objectMapper.readValue(elementString, MatrixElement.class);
        MatrixElement foundElement = movementService.findNextStep(element);
        Assertions.assertEquals('A', (char) foundElement.getContent());
    }

    @Test
    @DisplayName("Test 4 - record all elements of a map")
    void test04() throws Exception {
        Integer testIndex = 4;
        for (List<MatrixElement> row : matrix) {
            for (MatrixElement element : row) {
                recordingService.saveCurrentPosition(element);
            }
        }
        ChallengeResult result = recordingService.getResult();
        validateTestResult(testIndex, result, FolderEnum.RESULT_UNIT);
    }

    @PostConstruct
    void init() throws JsonProcessingException {
        String matrixString = TestBase.fetchFile(0, FolderEnum.INPUT_UNIT);
        matrix = objectMapper.readValue(matrixString, new TypeReference<List<List<MatrixElement>>>() {
        });
        movementService.setMatrix(matrix);
    }
}
