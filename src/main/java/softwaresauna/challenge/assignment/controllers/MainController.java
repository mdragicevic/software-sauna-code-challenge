package softwaresauna.challenge.assignment.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import softwaresauna.challenge.assignment.model.ChallengeError;
import softwaresauna.challenge.assignment.model.ChallengeResult;
import softwaresauna.challenge.assignment.services.MainService;


@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class MainController {

    private final MainService service;

    @PostMapping("/processMap")
    public ChallengeResult processMap(@RequestBody String body) throws ChallengeError {
        log.debug("processMap called");
        log.debug(body);
        return service.processInput(body);
    }

    @ExceptionHandler(ChallengeError.class)
    public ResponseEntity handleException(ChallengeError ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
