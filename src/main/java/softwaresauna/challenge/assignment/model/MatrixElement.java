package softwaresauna.challenge.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MatrixElement {
    Integer x;
    Integer y;
    Boolean passedThrough = false;
    DirectionEnum direction;
    Character content;
}
