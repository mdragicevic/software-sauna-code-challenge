package softwaresauna.challenge.assignment.model;

public class ChallengeError extends RuntimeException {
    public ChallengeError(String message) {
        super(message);
    }
}
