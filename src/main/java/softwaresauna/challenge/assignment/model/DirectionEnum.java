package softwaresauna.challenge.assignment.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Getter
@RequiredArgsConstructor
public enum DirectionEnum {

    UP(-1, 0, '|', Arrays.asList('|', '+', 'x')),
    DOWN(1, 0, '|', Arrays.asList('|', '+', 'x')),
    LEFT(0, -1, '-', Arrays.asList('-', '+', 'x')),
    RIGHT(0, 1, '-', Arrays.asList('-', '+', 'x'));

    private final Integer xChange;
    private final Integer yChange;
    private final Character symbol;
    private final List<Character> allowedNonLetterTargets;

    public static DirectionEnum turnLeft(DirectionEnum direction) {
        return switch (direction) {
            case UP -> LEFT;
            case DOWN -> RIGHT;
            case LEFT -> DOWN;
            case RIGHT -> UP;
        };
    }

    public static DirectionEnum turnRight(DirectionEnum direction) {
        return switch (direction) {
            case UP -> RIGHT;
            case DOWN -> LEFT;
            case LEFT -> UP;
            case RIGHT -> DOWN;
        };
    }
}
