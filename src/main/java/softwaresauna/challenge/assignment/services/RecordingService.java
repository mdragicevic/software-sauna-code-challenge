package softwaresauna.challenge.assignment.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import softwaresauna.challenge.assignment.model.ChallengeResult;
import softwaresauna.challenge.assignment.model.MatrixElement;

@Service
@Slf4j
public class RecordingService {

    String letters;
    String path;

    /**
     * a single step in the path, to be recorded for final response
     * @param currentPosition - an element of 2D map
     * @return true if this is the end of path, false if not end of path.
     */
    public boolean saveCurrentPosition(MatrixElement currentPosition) {
        Character character = currentPosition.getContent();
        if (character.equals('@')) {
            letters = "";
            path = "";
        }
        path += character;
        if (!currentPosition.getPassedThrough() && Character.isUpperCase(character)) {
            letters += character;
        }
        currentPosition.setPassedThrough(true);
        return !character.equals('x');
    }

    /**
     * returns challenge result, the final part of the algorithm
     * @return - ChallengeResult - letters and path
     */
    public ChallengeResult getResult() {
        return ChallengeResult.builder().letters(letters).path(path).build();
    }
}


