package softwaresauna.challenge.assignment.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import softwaresauna.challenge.assignment.model.ChallengeError;
import softwaresauna.challenge.assignment.model.ChallengeResult;
import softwaresauna.challenge.assignment.model.MatrixElement;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class MainService {
    private final MappingService mappingService;
    private final MovementService movementService;
    private final RecordingService recordingService;

    /**
     * main entry point method
     * @param text - map in string form
     * @return challenge result - path and letters
     * @throws ChallengeError - defined errors
     */
    public ChallengeResult processInput(String text) throws ChallengeError {
        List<List<MatrixElement>> matrix = mappingService.mapTextToMatrix(text);
        return traverseMatrix(matrix);
    }

    /**
     * traverse matrix, path defined by the challenge rules
     * @param matrix - represents map in list-of-lists form
     * @return challenge result - path and letters
     * @throws ChallengeError - defined errors
     */
    public ChallengeResult traverseMatrix(List<List<MatrixElement>> matrix) throws ChallengeError {
        movementService.setMatrix(matrix);
        MatrixElement currentPosition = movementService.findStartingPosition();
        boolean keepMoving;
        do {
            keepMoving = recordingService.saveCurrentPosition(currentPosition);
            if (keepMoving) {
                currentPosition = movementService.findNextStep(currentPosition);
            }
        } while (keepMoving);
        return recordingService.getResult();
    }


}


