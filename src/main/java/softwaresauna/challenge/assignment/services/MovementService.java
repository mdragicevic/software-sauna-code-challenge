package softwaresauna.challenge.assignment.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import softwaresauna.challenge.assignment.model.ChallengeError;
import softwaresauna.challenge.assignment.model.DirectionEnum;
import softwaresauna.challenge.assignment.model.MatrixElement;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MovementService {
    private List<List<MatrixElement>> matrix = new ArrayList<>();

    /**
     * sets matrix of values to be used by service.
     *
     * @param matrix
     */
    public void setMatrix(List<List<MatrixElement>> matrix) {
        this.matrix = matrix;
    }

    /**
     * find next step of the path, and define its direction.
     *
     * @param currentElement - current element
     * @return - next element
     * @throws ChallengeError
     */
    public MatrixElement findNextStep(MatrixElement currentElement) throws ChallengeError {
        MatrixElement nextElement = moveToNextPosition(currentElement, currentElement.getDirection());
        if (nextElement == null) {
            String errorMessage = "Broken path after character '" + currentElement.getContent() + "'";
            throw new ChallengeError(errorMessage);
        }
        determineNewDirection(currentElement, nextElement);
        return nextElement;
    }

    /**
     * find starting point of the path ('@') and check that there's only one starting position
     *
     * @return - element that contains starting point
     * @throws ChallengeError
     */
    public MatrixElement findStartingPosition() throws ChallengeError {
        List<MatrixElement> startingElementsFound = new ArrayList<>();
        matrix.forEach(row -> startingElementsFound.addAll(row.stream().filter(element -> element.getContent() == '@').toList()));
        if (startingElementsFound.size() == 1) {
            MatrixElement startingPosition = startingElementsFound.get(0);
            determineStartingDirection(startingPosition);
            return startingPosition;
        } else {
            String errorMessage = "Incorrect number of starting positions found. Required: 1, found: " + startingElementsFound.size();
            throw new ChallengeError(errorMessage);
        }
    }

    /**
     * determine the direction of path from starting position - make sure there is only 1 valid path.
     *
     * @param matrixElement - starting position
     * @throws ChallengeError
     */
    private void determineStartingDirection(MatrixElement matrixElement) throws ChallengeError {
        List<DirectionEnum> validDirections = new ArrayList<>();
        for (DirectionEnum direction : DirectionEnum.values()) {
            if (canMove(matrixElement, direction)) {
                validDirections.add(direction);
            }
        }
        if (validDirections.size() == 1) {
            matrixElement.setDirection(validDirections.get(0));
        } else if (validDirections.isEmpty()) {
            throw new ChallengeError("Found no valid starting path");
        } else {
            throw new ChallengeError("Found multiple starting paths");
        }
    }

    /**
     * determine direction of the next step, based on the previous step
     *
     * @param previousElement - last step
     * @param newElement      - new step
     */
    private void determineNewDirection(MatrixElement previousElement, MatrixElement newElement) {

        Character newContent = newElement.getContent();
        if (previousElement.getDirection().getSymbol().equals(newContent) || newContent.equals('x')) {
            newElement.setDirection(previousElement.getDirection());
        } else if (newContent.equals('+') || Character.isUpperCase(newContent)) {
            calculateNewDirectionOnTurn(previousElement.getDirection(), newElement);
        } else if (canGoStraightThroughIntersection(previousElement, newElement)) {
            newElement.setDirection(previousElement.getDirection());
        } else {
            String errorMessage = "Invalid end of path near character: " + newContent;
            throw new ChallengeError(errorMessage);
        }
        if (newElement.getDirection() == null) {
            String errorMessage = "Invalid end of path near character: " + newContent;
            throw new ChallengeError(errorMessage);
        }

    }

    /**
     * path can intersect itself and this is valid scenario.
     *
     * @param previousElement
     * @param newElement
     * @return
     */
    private boolean canGoStraightThroughIntersection(MatrixElement previousElement, MatrixElement newElement) {
        return elementsAreCrossroads(previousElement.getContent(), newElement.getContent()) && newElement.getPassedThrough();

    }

    /**
     * recognize that 2 characters are valid to be crossroads
     *
     * @param content1
     * @param content2
     * @return
     */
    private boolean elementsAreCrossroads(Character content1, Character content2) {
        List<Character> list = new ArrayList<>();
        list.add(content1);
        list.add(content2);
        return list.contains('|') && list.contains('-');
    }

    /**
     * '+' and letter can be a turn - calculate new direction on such turn.
     *
     * @param previousDirection - direction before the element
     * @param newElement        - element which is a turning point
     */
    private void calculateNewDirectionOnTurn(DirectionEnum previousDirection, MatrixElement newElement) {
        Character newContent = newElement.getContent();
        if (Character.isUpperCase(newContent)) {
            calculateNewDirectionOnLetter(previousDirection, newElement);
        }
        if (newContent.equals('+')) {
            calculateNewDirectionOnPlus(previousDirection, newElement);
        }
    }

    /**
     * letter can be a turn - direction can change on each letter
     *
     * @param previousDirection - direction before this element
     * @param newElement
     */
    private void calculateNewDirectionOnLetter(DirectionEnum previousDirection, MatrixElement newElement) {
        MatrixElement elementAhead = moveToNextPosition(newElement, previousDirection);
        if (elementAhead != null) {
            if (previousDirection.getAllowedNonLetterTargets().contains(elementAhead.getContent()) ||
                    Character.isUpperCase(elementAhead.getContent())) {
                newElement.setDirection(previousDirection);
            }
        }
        if (newElement.getDirection() == null) {
            DirectionEnum turn = elementIsATurn(previousDirection, newElement);
            newElement.setDirection(turn);
        }
    }

    /**
     * '+' represents a turn. find new direction. if there is no direction change - throw error
     *
     * @param previousDirection
     * @param newElement
     */
    private void calculateNewDirectionOnPlus(DirectionEnum previousDirection, MatrixElement newElement) {
        MatrixElement elementAhead = moveToNextPosition(newElement, previousDirection);
        if (elementAhead != null) {
            if (previousDirection.getAllowedNonLetterTargets().contains(elementAhead.getContent()) ||
                    Character.isUpperCase(elementAhead.getContent())) {
                throw new ChallengeError("Fake turn found");
            }
        }
        DirectionEnum turn = elementIsATurn(previousDirection, newElement);
        newElement.setDirection(turn);
    }

    /**
     * move to next position on the path
     *
     * @param element   current element from which we are moving
     * @param direction - direction in which we are moving
     * @return - new element that we reached
     */
    private MatrixElement moveToNextPosition(MatrixElement element, DirectionEnum direction) {
        int newX = element.getX() + direction.getXChange();
        int newY = element.getY() + direction.getYChange();
        if (newX < 0 || newY < 0) {
            return null;
        }
        if (newX > this.matrix.size() - 1 || newY > this.matrix.get(newX).size() - 1) {
            return null;
        }
        return this.matrix.get(element.getX() + direction.getXChange()).get(element.getY() + direction.getYChange());
    }

    /**
     * validate we are not going out of bounds of the matrix (list of lists)
     *
     * @param x
     * @param y
     * @return
     */
    Boolean checkIfNewValuesAreInsideLimits(Integer x, Integer y) {
        if (x < 0 || y < 0) {
            return false;
        } else return x <= this.matrix.size() - 1 && y <= this.matrix.get(x).size() - 1;
    }

    /**
     * look for valid path by turning left or right. Throw error if we can do neither or both.
     *
     * @param previousDirection
     * @param element
     * @return
     * @throws ChallengeError
     */
    private DirectionEnum elementIsATurn(DirectionEnum previousDirection, MatrixElement element) throws ChallengeError {
        List<DirectionEnum> allowedDirections = new ArrayList<>();
        DirectionEnum turnLeft = DirectionEnum.turnLeft(previousDirection);
        int newX = element.getX() + turnLeft.getXChange();
        int newY = element.getY() + turnLeft.getYChange();
        boolean valid = checkIfNewValuesAreInsideLimits(newX, newY);
        if (valid) {
            MatrixElement elementLeft = this.matrix.get(newX).get(newY);
            if (turnLeft.getAllowedNonLetterTargets().contains(elementLeft.getContent()) ||
                    Character.isUpperCase(elementLeft.getContent())) {
                allowedDirections.add(turnLeft);
            }
        }

        DirectionEnum turnRight = DirectionEnum.turnRight(previousDirection);
        newX = element.getX() + turnRight.getXChange();
        newY = element.getY() + turnRight.getYChange();
        valid = checkIfNewValuesAreInsideLimits(newX, newY);
        if (valid) {
            MatrixElement elementRight = this.matrix.get(newX).get(newY);
            if (turnRight.getAllowedNonLetterTargets().contains(elementRight.getContent()) ||
                    Character.isUpperCase(elementRight.getContent())) {
                allowedDirections.add(turnRight);
            }
        }

        if (allowedDirections.size() == 1) {
            return allowedDirections.get(0);
        } else {
            String errorMessage;
            if (allowedDirections.isEmpty()) {
                errorMessage = "Error - no valid path found on character '" + element.getContent() + "'.";
            } else {
                errorMessage = "Error - multiple valid paths found on character '" + element.getContent() + "'.";
            }
            throw new ChallengeError(errorMessage);
        }
    }

    /**
     * check if movement is valid, including validating matrix bounds
     *
     * @param matrixElement
     * @param direction
     * @return
     */
    private boolean canMove(MatrixElement matrixElement, DirectionEnum direction) {
        Integer x = matrixElement.getX();
        Integer y = matrixElement.getY();
        if (direction == DirectionEnum.LEFT && y == 0) {
            return false;
        }
        if (direction == DirectionEnum.RIGHT && y >= matrix.get(x).size() - 1) {
            return false;
        }
        if (direction == DirectionEnum.UP) {
            if (x == 0) {
                return false;
            }
            if (y >= matrix.get(x - 1).size() - 1) {
                return false;
            }
        }
        if (direction == DirectionEnum.DOWN) {
            if (x >= matrix.size() - 1) {
                return false;
            }
            if (y > matrix.get(x + 1).size() - 1) {
                return false;
            }
        }
        return checkIfValidMovement(matrixElement, direction);
    }

    /**
     * check if next element candidate is a valid target.
     *
     * @param matrixElement
     * @param direction
     * @return
     */
    private Boolean checkIfValidMovement(MatrixElement matrixElement, DirectionEnum direction) {
        Integer x = matrixElement.getX();
        Integer y = matrixElement.getY();
        boolean isValid = false;
        Character newCharacter = this.matrix.get(x + direction.getXChange()).get(y + direction.getYChange()).getContent();
        if (Character.isUpperCase(newCharacter) || direction.getAllowedNonLetterTargets().contains(newCharacter)) {
            isValid = true;
        }
        return isValid;
    }

}


