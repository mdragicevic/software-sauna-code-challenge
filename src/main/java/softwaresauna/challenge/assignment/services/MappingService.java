package softwaresauna.challenge.assignment.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import softwaresauna.challenge.assignment.model.MatrixElement;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class MappingService {
    /**
     * transform input text into a matrix for algorithm to use
     * @param text input from outside
     * @return list of lists of fields - a 2D map
     */
    public List<List<MatrixElement>> mapTextToMatrix(String text) {
        String[] lines = text.split(System.lineSeparator());
        List<List<MatrixElement>> output = new ArrayList<>();
        Integer rowCounter = 0;
        for (String line : lines) {
            char[] chars = line.toCharArray();
            List<MatrixElement> rowArray = new ArrayList<>();
            int columnCounter = 0;
            for (char character : chars) {
                MatrixElement newElement = MatrixElement.builder()
                        .x(rowCounter)
                        .y(columnCounter++)
                        .passedThrough(false)
                        .content(character).build();
                rowArray.add(newElement);
            }
            output.add(rowArray);
            rowCounter++;
        }
        return output;
    }
}


