## Software Sauna Code Challenge
### Technologies
Project was made using
* Spring Boot 2.6.7
* Java 17 (Eclipse Temurin 17.0.3 JDK)
* Gradle 7.4.1
* IntelliJ IDEA IDE 2022.1.1

### Model
Few simple classes created for the model
* ChallengeError - an exception, thrown for all error responses (invalid maps)
* ChallengeResult - represents successful output - contains letters and path, as per assignment rules
* MatrixElement - represents a single field of the map
* DirectionEnum - we can move UP/DOWN/LEFT/RIGHT through the matrix, and this enum is used to define that movement on each element.

### Services
The algorithm is split in services
* MainService - single entry point for acceptance tests and controller
* MappingService - maps input map (a string) into a 2D matrix (List of Lists)
* MovementService - contains methods that define how program "moves" through the path step-by-step and throws errors if path is invalid
* RecordingService - records path passed - for output.

### AcceptanceTests
* every test has input (defined in src\test\resources\inputs), and the expected output (defined in src\test\resources\results)
* all 15 test are exact copy of valid/invalid maps given in code challenge.
* every test is made to assert that the actual result equals the expected result. 

### UnitTests
* few tests to test the behaviour of parts of the alghoritm - public methods of subservices.

### Controller
* besides writing more tests, we can boot the spring application, send inputs over http and get output as response.
* POST REQUEST http://localhost:8080/processMap
* postman_collection.json - postman collection that can be used for sending request
### Note
* non-valid characters (for example, lower-case letters), are not allowed as part of path, but are allowed to appear on map
### Contact
miroslav.dragicevic@gmail.com
